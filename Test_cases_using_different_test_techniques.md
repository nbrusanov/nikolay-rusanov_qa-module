Test cases using different test techniques - forum topic creation


Test description                                        Priority                       Techniques

1. For the forum post formatting I can use:             Prio4                          Pairwise testing
- bold and italic
- bold and underlined
- italic and underlined

2. As a user I should be able to create a               Prio1                          Use case testing
topic with title and some text

3. As a user I should be able to see my created         Prio1                          Use case testing
topic

4. As a user I should be able to modify my              Prio1                          Use case testing
created topic - title and forum post

5. As a user I should see topics created by             Prio1                          Use case testing
other users

6. As a user I should be able to reply to               Prio1                          Use case testing
topics created by other users

7. If never seen the forum before try to                Prio1                          Exploratory testing
register, login and create a new topic in 5 minutes

8. Logged in user - New topic creation in progress -    Prio1                          State transition 
New topic created and visible for all registerred 
users 

9. User subscribed for notifications (Y/N) -            Prio3                          Decision tables 
   User subsribed for notifications when 
   replied to their post (Y/N) -
   User already saw all new posts (Y/N) 

10. User creates a new topic with set                   Prio1                          Equivalence partitioning 
characters for the title and the forum post -
all valid numbers   

11. User try to create a new topic with 0               Prio2                          Boundary values
characters for the title and the forum post

12. User try to create a new topic with the             Prio1                          Boundary values
maximum allowed characters for the title and 
the forum post

13. User try to create a new topic with the             Prio1                          Boundary values
maximum allowed characters plus one
for the title  and the forum post

14. User try to create a new topic with                 Prio2                          Classification tree      
valid title, short text and using one feature
from all options for text formatting - for 
expample bold text, text with specific font, 
text with specific size, text with specific 
color, one emoticon. 

