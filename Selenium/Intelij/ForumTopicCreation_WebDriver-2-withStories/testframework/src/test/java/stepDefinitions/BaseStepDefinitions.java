package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
//import pages.bing.ForumHomePage;
//import pages.bing.BingResultsPage;

public class BaseStepDefinitions {

    UserActions actions = new UserActions ();
//    ForumHomePage homePage = new ForumHomePage (actions.getDriver ());
//    BingResultsPage resultsPage = new BingResultsPage (actions. getDriver ());

    @BeforeStory
    public void setUp () {
        UserActions.loadBrowser("forum.homePage");
    }

    @AfterStory
    public static void tearDown() {
        UserActions.quitDriver ();
    }
}
