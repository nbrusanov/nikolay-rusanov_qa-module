package testCases.bing;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.forum.*;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ForumTopicCreationTest extends BaseTest {

    @Before
    public void setUpWait () {
        actions.getDriver ().manage ().timeouts ().implicitlyWait (10, TimeUnit.SECONDS);
    }

    @Test
    public void loadAuthenticationPage() {
        // Arrange, Act
        ForumHomePage forumHomePage = new ForumHomePage (actions.getDriver ());
        forumHomePage.clickOnLoginButton ();

        // Assert
        forumHomePage.assertAuthenticationPageIsLoaded ();
    }

    @Test
    public void authenticateToForumWithValidCredentials() {
        // Arrange
        loadAuthenticationPage ();

        // Act
        AuthenticationPage authenticationPage = new AuthenticationPage (actions.getDriver ());
        authenticationPage.typeValidCredentialsAndSubmit ("pencho@bogdanovi.com", "Pen40NeCheti");
        MainPageWithLoggedInUser mainPageWithLoggedInUser = new MainPageWithLoggedInUser (actions.getDriver ());

        // Assert
        Assert.assertTrue ("User avatar is not visible",
                mainPageWithLoggedInUser.getUserAvatar ().isDisplayed ());

    }

    @Test
    public void createANewTopicWithTitleCategoryTagsAndText_whenUserIsAuthenticated() {
        // Arrange
        authenticateToForumWithValidCredentials ();

        // Act
        MainPageWithLoggedInUser mainPageWithLoggedInUser = new MainPageWithLoggedInUser (actions.getDriver ());
        mainPageWithLoggedInUser.clickOnNewTopic ();
        NewTopicFormPage newTopicFormPage = new NewTopicFormPage (actions.getDriver ());
        newTopicFormPage.assertTextFieldIsVisible ();
        newTopicFormPage.assertTextFieldIsEnabled ();
        String uuid = UUID.randomUUID ().toString ();
        newTopicFormPage.createATopicWithTitleCategoryTagsAndTextAndClickCreateTopic ("This is a test topic",
                "fun", "This is a text for test purposes" + uuid);

        // Assert
        CreatedTopicPage createdTopicPage = new CreatedTopicPage (actions.getDriver ());
        createdTopicPage.assertTopicIsCreated ();
    }
}


