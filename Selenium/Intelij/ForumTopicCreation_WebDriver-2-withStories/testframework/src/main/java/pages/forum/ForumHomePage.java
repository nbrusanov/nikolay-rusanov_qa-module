package pages.forum;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class ForumHomePage extends BasePage {

    public ForumHomePage(WebDriver driver) {
        super(driver, "forum.homePage");
    }

    public void clickOnLoginButton (){
        actions.clickElement ("forum.homePage.loginButton");
    }

    public void assertAuthenticationPageIsLoaded () {
        Assert.assertTrue ("The desired url for authentication page was not loaded", driver.getCurrentUrl ().
                contains (Utils.getConfigPropertyByKey("forum.authenticationPage")));
    }
}
