package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewTopicFormPage extends BasePage {

    public NewTopicFormPage(WebDriver driver) {
        super (driver, "forum.mainPage");
    }

    public void createATopicWithTitleCategoryTagsAndTextAndClickCreateTopic(String title, String searchTerm, String topicContent) {
        actions.typeValueInField (title, "forum.newTopicFormPage.titleField");
        actions.clickElement ("forum.newTopicFormPage.categoryMenu");
        actions.typeValueInField (searchTerm, "forum.newTopicFormPage.searchFieldForCategory");
        actions.clickElement ("forum.newTopicFormPage.funCategoryBtn");
        actions.clickElement ("forum.newTopicFormPage.tagsBtn");
        actions.clickElement ("forum.newTopicFormPage.preparationTagBtn");
        actions.clickElement ("forum.newTopicFormPage.importantTagBtn");
        actions.clickElement ("forum.newTopicFormPage.organisationalTagBtn");
        actions.typeValueInField (topicContent, "forum.newTopicFormPage.textArea");
        actions.clickElement ("forum.newTopicFormPage.createNewTopicBtn");
    }

    public WebElement getTextArea () {
        return driver.findElement (By.tagName ("textarea"));
    }

    public void assertTextFieldIsVisible () {
        Assert.assertTrue ("Text area is not visible", getTextArea ().isDisplayed ());
    }

    public void assertTextFieldIsEnabled () {
        Assert.assertTrue ("Text area is not visible", getTextArea ().isEnabled ());
    }

}
