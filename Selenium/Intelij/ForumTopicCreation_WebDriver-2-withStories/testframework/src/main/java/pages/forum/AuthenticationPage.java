package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class AuthenticationPage extends BasePage {

    public AuthenticationPage(WebDriver driver) {
        super(driver, "forum.authenticationPage");
    }

    public void typeValidCredentialsAndSubmit (String email, String password) {
        actions.typeValueInField (email, "forum.authenticationPage.emailField");
        actions.typeValueInField (password, "forum.authenticationPage.passwordField");
        actions.clickElement ("forum.authenticationPage.signInBtn");
    }

    public void assertAuthenticationPageIsLoaded () {
        actions.assertNavigatedUrl ("forum.mainPage");
    }
}
