package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPageWithLoggedInUser extends BasePage {

    public MainPageWithLoggedInUser(WebDriver driver) {
        super (driver, "forum.mainPage");
    }

    public WebElement getUserAvatar() {
        return driver.findElement (By.cssSelector ("#current-user .avatar"));
    }

    public void clickOnNewTopic() {
        actions.clickElement ("forum.mainPage.createNewTopicBtn");
    }

}

