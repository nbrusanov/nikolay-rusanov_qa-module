package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class CreatedTopicPage extends BasePage {

    public CreatedTopicPage(WebDriver driver) {
        super (driver, "forum.createdtopicPage");
    }

    public void assertTopicIsCreated () {
        actions.assertElementPresent ("forum.createdTopicPage");
    }

}
