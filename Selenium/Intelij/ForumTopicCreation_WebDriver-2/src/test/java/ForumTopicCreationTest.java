import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.AuthenticationPage;
import pages.ForumHomePage;
import pages.MainPageWithLoggedInUser;
import pages.NewTopicFormPage;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ForumTopicCreationTest {

    private WebDriver webdriver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver ().setup ();
    }

    @Before
    public void setup() {
        webdriver = new ChromeDriver ();
        webdriver.manage ().timeouts ().implicitlyWait (10, TimeUnit.SECONDS);
        webdriver.get ("https://stage-forum.telerikacademy.com/");
    }

    @After
    public void tearDown() {
        webdriver.quit ();
    }

    @Test
    public void loadAuthenticationPage() {
        // Arrange, Act
        ForumHomePage forumHomePage = new ForumHomePage (webdriver);
        forumHomePage.clickOnLogin ();

        // Act
        Assert.assertTrue ("The desired url for authentication page was not loaded", webdriver.getCurrentUrl ().
                contains ("https://auth.telerikacademy.com/account/login"));
    }

    @Test
    public void authenticateToForumWithValidCredentials() {
        // Arrange
        loadAuthenticationPage ();

        // Act
        AuthenticationPage authenticationPage = new AuthenticationPage (webdriver);
        authenticationPage.typeValidCredentialsAndSubmit ("pencho@bogdanovi.com", "Pen40NeCheti");
        MainPageWithLoggedInUser mainPageWithLoggedInUser = new MainPageWithLoggedInUser (webdriver);

        // Assert
        Assert.assertTrue ("User avatar is not visible",
                mainPageWithLoggedInUser.getUserAvatar ().isDisplayed ());
    }

    @Test
    public void createANewTopicWithTitleCategoryTagsAndText_whenUserIsAuthenticated() {
        // Arrange
        authenticateToForumWithValidCredentials ();

        // Act
        MainPageWithLoggedInUser mainPageWithLoggedInUser = new MainPageWithLoggedInUser (webdriver);
        mainPageWithLoggedInUser.clickOnNewTopic ();
        NewTopicFormPage newTopicFormPage = new NewTopicFormPage (webdriver);
        Assert.assertTrue ("The input field for forum post is not displayed",
                newTopicFormPage.getTextArea ().isDisplayed ());
        Assert.assertTrue ("The input field for forum post is not enabled",
                newTopicFormPage.getTextArea ().isEnabled ());
        String uuid = UUID.randomUUID ().toString ();
        newTopicFormPage.createATopicWithTitleCategoryTagsAndTextAndClickCreateTopic ("This is a test topic",
                "fun", "This is a text for test purposes" + uuid);

        // Assert
        WebElement topicContent = webdriver.findElement
                (By.xpath ("//div[@class=\"regular category-alpha-preparation-fun tag-preparation tag-important tag-organisational ember-view\"]"));
        Assert.assertTrue ("Topic was not created", topicContent.isDisplayed ());
    }

}

