package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumHomePage {

    private WebDriver driver;

    public ForumHomePage(WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getSiteLogo () {
        return driver.findElement (By.id ("site-logo"));
    }

    public WebElement getLearningPlatformLink () {
        return driver.findElement (By.xpath ("//a[@title=\"Learning Platform\"]"));
    }

    public WebElement getLoginButton () {
        return driver.findElement (By.xpath ("//button[contains(@class, \"login-button\")]"));
    }

    public WebElement getSearchByTopicsUsersOrCategoriesButton () {
        return driver.findElement (By.xpath ("/a[@title=\"search topics, posts, users, or categories\"]"));
    }

    public void clickOnLogin () {
        getLoginButton ().click ();
    }





}
