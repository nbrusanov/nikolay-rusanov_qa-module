package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPageWithLoggedInUser {

    private WebDriver driver;

    public MainPageWithLoggedInUser (WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getUserAvatar () {
        return driver.findElement (By.cssSelector ("#current-user .avatar"));
    }

    public WebElement getPrivateMessagesButton () {
        return driver.findElement (By.id ("toggle-messages-menu"));
    }

    public WebElement getNewTopicButton () {
        return driver.findElement (By.id ("create-topic"));
    }

    public void clickOnNewTopic () {
        getNewTopicButton ().click ();
    }

}
