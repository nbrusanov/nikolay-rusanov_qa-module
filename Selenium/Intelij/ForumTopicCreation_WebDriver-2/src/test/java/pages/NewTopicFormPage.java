package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewTopicFormPage {
    private WebDriver driver;

    public NewTopicFormPage(WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getCreateNewPrivateMessageButton() {
        return driver.findElement (By.xpath ("//div/button[@aria-expanded=\"true\"]"));
    }

    public WebElement getTitleField() {
        return driver.findElement (By.id ("reply-title"));
    }

    public WebElement getCategoryMenu() {
        return driver.findElement (By.xpath ("//*[@title=\"Uncategorized\"]/parent::div"));
    }

    public WebElement getSearchField() {
        return driver.findElement (By.xpath ("//input[contains(@id,\"ember\")]"));
    }

    public WebElement getFunCategory() {
        return driver.findElement (By.xpath ("//li[@data-name=\"Fun\"]"));
    }

    public WebElement getTags () {
        return driver.findElement (By.xpath ("//div[contains(@class,\"title-and-category\")]//div[contains(@class,\"mini-tag-chooser\")]"));
    }

    public WebElement getPreparationTag () {
        return driver.findElement (By.xpath ("//li[@title=\"preparation\"]"));
    }

    public WebElement getImportantTag () {
        return driver.findElement (By.xpath ("//li[@title=\"important\"]"));
    }

    public WebElement getOrganisationalTag () {
        return driver.findElement (By.xpath ("//li[@title=\"organisational\"]"));
    }

    public WebElement getTextArea () {
        return driver.findElement (By.tagName ("textarea"));
    }

    public WebElement getCreateTopicButton () {
        return driver.findElement (By.xpath ("//button[@aria-label=\"Create Topic\"]"));
    }

    public void createATopicWithTitleCategoryTagsAndTextAndClickCreateTopic (String title, String searchTerm, String topicContent) {
        getTitleField ().sendKeys (title);
        getCategoryMenu ().click ();
        getSearchField ().sendKeys (searchTerm);
        getFunCategory ().click ();
        getTags ().click ();
        getPreparationTag ().click ();
        getImportantTag ().click ();
        getOrganisationalTag ().click ();
        getTextArea ().sendKeys (topicContent);
        getCreateTopicButton ().click ();
    }

}
