package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AuthenticationPage {

    private WebDriver driver;

    public AuthenticationPage(WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getMainSinUpButton() {
        return driver.findElement (By.xpath ("//div[@class=\"login-header\"]/a"));
    }

    public WebElement getFieldForEmail() {
        return driver.findElement (By.xpath ("//input[@id=\"Email\"]"));
    }

    public WebElement getFieldForPassword() {
        return driver.findElement (By.xpath ("//input[@id=\"Password\"]"));
    }

    public WebElement getSignInButton() {
        return driver.findElement (By.id ("next"));
    }

    public WebElement getSignInWithTelerikAcademyButton() {
        return driver.findElement (By.id ("TelerikAcademyAD"));
    }

    public WebElement getAlternativeSignUpButton() {
        return driver.findElement (By.id ("createAccount"));
    }

    public void typeValidCredentialsAndSubmit(String email, String password) {
        getFieldForEmail ().sendKeys (email);
        getFieldForPassword ().sendKeys (password);
        getSignInButton ().click ();
    }

}

