import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BingTest {

    @Test
    public void navigateToBing() {
        System.setProperty ("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver ();

        // Arrange
        webdriver.get ("https://bing.com");

        // Act
        WebElement searchInput = webdriver.findElement (By.id ("sb_form_q"));
//        WebElement searchInput = webdriver.findElement (By.name ("q"));
//        WebElement searchInput = webdriver.findElement (By.className ("sb_form_q"));
//        WebElement searchInput = webdriver.findElement (By.xpath ("//input[@id='sb_form_q']"));

        // Assert
        Assert.assertTrue ("Search input was not displayed.", searchInput.isDisplayed ());
        webdriver.close ();
    }

    @Test
    public void searchForTerm() {
        System.setProperty ("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver ();

        // Arrange
        webdriver.get ("https://bing.com");

        // Act
        WebElement searchInput = webdriver.findElement (By.name ("q"));
        searchInput.sendKeys ("Telerik Academy Alpha");

        WebElement searchButton = webdriver.findElement (By.xpath ("//label[@for='sb_form_go']"));
        searchButton.click ();

        WebElement resultsList = webdriver.findElement (By.id ("b_results"));

        // List<WebElement> results = resultsList.findElements (By.tagName ("li"));
        List<WebElement> results = resultsList.findElements (By.xpath (".//li"));

        // Assert
        Assert.assertTrue ("Results not found", results.get (0).isDisplayed ());

        webdriver.close ();
    }

    public void navigateToBing_waitElementByLinkText () {
        System.setProperty ("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver ();

        // Add implicit wait
        //webdriver.manage().timeouts ().implicitlyWait (5, TimeUnit.SECONDS);

        // Add Explicit Wait
        WebDriverWait wait = new WebDriverWait (webdriver, 10);

        // Arrange
        webdriver.get ("https://bing.com");

        // Act
        // Set Explicit wait Conditions
        wait.until(ExpectedConditions.visibilityOfElementLocated (By.linkText ("Images")));

        WebElement imagesLink = webdriver.findElement (By.linkText ("Images"));

        // Assert
        Assert.assertTrue ("Images link was not displayed", imagesLink.isDisplayed ());

        webdriver.close ();

    }
}



