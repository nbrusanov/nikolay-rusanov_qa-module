import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ForumTopicCreationTest {

    private WebDriver webdriver;

    // TODO - change from path to universal (Boni Garcia)
    @Before
    public void setup() {
        System.setProperty ("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
        webdriver = new ChromeDriver ();
        webdriver.manage ().timeouts ().implicitlyWait (10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        webdriver.quit ();
    }

    @Test
    public void navigateToForum() throws InterruptedException {
        // Arrange

        // Open forum
        webdriver.get ("https://stage-forum.telerikacademy.com/");

        // Act

        // Login
        WebElement loginBtn = webdriver.findElement (By.xpath ("//button[contains(@class, \"login-button\")]"));
        loginBtn.click ();
        WebElement eMailForLogIn = webdriver.findElement (By.id ("Email"));
        eMailForLogIn.sendKeys ("pencho@bogdanovi.com");
        WebElement password = webdriver.findElement (By.id ("Password"));
        password.sendKeys ("Pen40NeCheti");
        WebElement signInBtn = webdriver.findElement (By.id ("next"));
        signInBtn.click ();

        // Create topic with category, tags and text
        WebElement createTopicBtn = webdriver.findElement (By.id ("create-topic"));
        createTopicBtn.click ();

        WebElement titleField = webdriver.findElement (By.id ("reply-title"));
        titleField.sendKeys ("This is a test topic");

        WebElement categoryMenu = webdriver.findElement (By.xpath ("//*[@title=\"Uncategorized\"]/parent::div"));
        categoryMenu.click ();

        WebElement searchField = webdriver.findElement (By.xpath ("//input[contains(@id,\"ember\")]"));
        searchField.sendKeys ("fun");

        WebElement funCategory = webdriver.findElement (By.xpath ("//li[@data-name=\"Fun\"]"));
        funCategory.click ();

        WebElement tags = webdriver.findElement (By.xpath ("//div[contains(@class,\"title-and-category\")]//div[contains(@class,\"mini-tag-chooser\")]"));
        tags.click ();

        WebElement preparationTag = webdriver.findElement (By.xpath ("//li[@title=\"preparation\"]"));
        preparationTag.click ();

        WebElement importantTag = webdriver.findElement (By.xpath ("//li[@title=\"important\"]"));
        importantTag.click ();

        WebElement organisationalTag = webdriver.findElement (By.xpath ("//li[@title=\"organisational\"]"));
        organisationalTag.click ();

        String uuid = UUID.randomUUID().toString();
        WebElement textArea = webdriver.findElement (By.tagName ("textarea"));
        textArea.sendKeys ("This is a text for test purposes" + uuid);

        WebElement createTopic = webdriver.findElement (By.xpath ("//button[@aria-label=\"Create Topic\"]"));
        createTopic.click ();

        //Assert

        // Check if the topic is created
        WebElement topicContent = webdriver.findElement (By.xpath ("//div[@class=\"regular category-alpha-preparation-fun tag-preparation tag-important tag-organisational ember-view\"]"));
        Assert.assertTrue ("Topic was not created", topicContent.isDisplayed ());

    }

}
